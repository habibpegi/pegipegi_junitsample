package com.pegipegi.training.test.unit.client;

import java.util.List;

public interface TodoClient {

	public List<String> retrieveTodos(String user);

	void deleteTodo(String todo);

}
