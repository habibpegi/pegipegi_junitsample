package com.pegipegi.training.test.unit.service;

import java.util.ArrayList;
import java.util.List;

import com.pegipegi.training.test.unit.client.TodoClient;

public class TodoService {

	private TodoClient client;

	public TodoService(TodoClient client) {
		this.client = client;
	}

	public List<String> retrieveTodosWithCriteria(String user, String criteria) {
		List<String> filteredTodos = new ArrayList<String>();
		List<String> allTodos = client.retrieveTodos(user);
		for (String todo : allTodos) {
			if (todo.contains(criteria)) {
				filteredTodos.add(todo);
			}
		}
		return filteredTodos;
	}
	
	public void deleteTodosWithCriteria(String user, String criteria) {
		List<String> allTodos = client.retrieveTodos(user);
		for (String todo : allTodos) {
			if (todo.contains(criteria)) {
				client.deleteTodo(todo);
			}
		}
	}

}
