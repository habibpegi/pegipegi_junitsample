package com.pegipegi.training.test.unit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/*
 * Ignore penamaan class dan method! 
 */

public class JunitAnnotation {

	//execute before class
	@BeforeClass
	public static void beforeClass() {
		System.out.println("before class");
	}

	//execute after class
	@AfterClass
	public static void  afterClass() {
		System.out.println("after class");
	}

	//execute before test
	@Before
	public void before() {
		System.out.println("before test");
	}

	//execute after test
	@After
	public void after() {
		System.out.println("after test");
	}

	//test case
	@Test
	public void test() {
		System.out.println("test");
	}

	//test case ignore and will not execute
	@Ignore
	public void ignoreTest() {
		System.out.println("ignore test");
	}
	
	@Test(expected = NumberFormatException.class)
	public void testNumberFormatExceptionThrows() {
		// logic yang akan melempar NumberFormatException
		throw new NumberFormatException();
	}
	
	@Test(timeout = 100)
	public void testTimeExecutionLowerThan100msPass() {
		// logic yang waktu eksekusinya <= 100
		try {
			Thread.sleep(60);
		} catch (InterruptedException e) {
			
		}
	}

}
