package com.pegipegi.training.test.unit.client.stub;

import java.util.Arrays;
import java.util.List;

import com.pegipegi.training.test.unit.client.TodoClient;

/*
 * Ignore penamaan class dan method! 
 */
public class TodoClientStub implements TodoClient {

	public List<String> retrieveTodos(String user) {
		return Arrays.asList("Learn JUnit", "Learn Stub", "Learn Mock");
	}

	public void deleteTodo(String todo) {}

}
