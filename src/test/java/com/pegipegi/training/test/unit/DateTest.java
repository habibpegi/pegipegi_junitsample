package com.pegipegi.training.test.unit;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class DateTest {
	
	// ada yang salah?
	@Test
	public void testGetYear() {
		assertEquals(2018, LocalDate.of(2018, 11, 23).getYear());
	}

}
