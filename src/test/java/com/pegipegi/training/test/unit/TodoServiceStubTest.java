package com.pegipegi.training.test.unit;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.pegipegi.training.test.unit.client.TodoClient;
import com.pegipegi.training.test.unit.client.stub.TodoClientStub;
import com.pegipegi.training.test.unit.service.TodoService;

/*
 * Ignore penamaan class dan method! 
 */
public class TodoServiceStubTest {

	@Test
	public void usingStub() {
		TodoClient client = new TodoClientStub();
		TodoService service = new TodoService(client);
		List<String> todos = service.retrieveTodosWithCriteria("Dummy", "Mock");
		assertEquals(1, todos.size());
	}
}
