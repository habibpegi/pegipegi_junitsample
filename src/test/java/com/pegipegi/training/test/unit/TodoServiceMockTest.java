package com.pegipegi.training.test.unit;


import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mockito;

import com.pegipegi.training.test.unit.client.TodoClient;
import com.pegipegi.training.test.unit.service.TodoService;

/*
 * Ignore penamaan class dan method! 
 */

@Category(com.pegipegi.training.test.unit.FastTest.class)
public class TodoServiceMockTest {

	@Test
	public void usingMock() {

		TodoClient client = mock(TodoClient.class);
		TodoService service = new TodoService(client);
		List<String> todos = Arrays.asList("Learn JUnit", "Learn Stub", "Learn Mock");

		//given
		given(client.retrieveTodos("Dummy")).willReturn(todos);

		//when
		List<String> todoss = service.retrieveTodosWithCriteria("Dummy", "Mock");

		//then
		assertThat(todoss.size(), is(1));
		
	}
	
	@Test
	public void testDeleteTodosWithCriteriaTodoExistDeleted() {

		TodoClient client = mock(TodoClient.class);

		List<String> todos = Arrays.asList("Learn JUnit", "Learn Stub", "Learn Mock");

		when(client.retrieveTodos("Dummy")).thenReturn(todos);

		TodoService service = new TodoService(client);

		service.deleteTodosWithCriteria("Dummy", "Mock");

		verify(client, Mockito.times(1)).deleteTodo("Learn Mock");

	}

}
