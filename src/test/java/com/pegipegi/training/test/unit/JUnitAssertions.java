package com.pegipegi.training.test.unit;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/*
 * Ignore penamaan class dan method! 
 */
public class JUnitAssertions {

	String str1;
	String str2;
	String str3;
	String str4;
	String str5;

	int val1;
	int val2;

	String[] expectedArray;
	String[] resultArray;


	// don't do this!
	@Before
	public void setup() {
		str1 = new String ("abc");
		str2 = new String ("abc");
		str3 = null;
		str4 = "abc";
		str5 = "abc";

		val1 = 5;
		val2 = 6;

		expectedArray = new String[] {"one", "two", "three"};
		resultArray =  new String[] {"one", "two", "three"};
	}

	// multiple assert in one method is wrong!
	@Test
	public void testAssertions() {
		//Check that two objects are equal
		assertEquals(str1, str2);

		//Check that a condition is true
		assertTrue (val1 < val2);

		//Check that a condition is false
		assertFalse(val1 > val2);

		//Check that an object isn't null
		assertNotNull(str1);

		//Check that an object is null
		assertNull(str3);

		//Check if two object references point to the same object
		assertSame(str4,str5);

		//Check if two object references not point to the same object
		assertNotSame(str1,str3);

		//Check whether two arrays are equal to each other.
		assertArrayEquals(expectedArray, resultArray);
	}

}
